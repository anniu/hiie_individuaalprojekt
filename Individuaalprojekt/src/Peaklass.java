
import java.util.ArrayList;
import java.util.Collections;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;

public class Peaklass extends Application {
	
	public static int n=10;
	public static double probability=0.5;
	

	@Override
	public void start(Stage primaryStage) {
		
		final GridPane grid=new GridPane();
		grid.setHgap(20);
        grid.setVgap(20);    
        
        final Object green="-fx-background-color: green;";
        final Object yellow="-fx-background-color: yellow;";
        final Object red="-fx-background-color: red;";
        final Object black="-fx-background-color: black;";
        
        final ArrayList<ArrayList<Object>> grid_matrix=create_matrix(n,green,yellow,red);
        
        n=n+2;

        write_grid(grid,grid_matrix);
        
        Button button=new Button("Next");
        grid.add(button,n+1,n+1); 
        
        Scene scene = new Scene(grid,500,500);
        
        primaryStage.setTitle("Fire"); 
        primaryStage.setScene(scene);
        primaryStage.show();
        
        button.setOnAction(new EventHandler<ActionEvent>() {
            public void handle(ActionEvent ae) {  	
        		spread(grid_matrix,green,yellow,red,black);
                for(int i=0;i<n;i++){
                	write_grid(grid,grid_matrix);
                }
        	}
        });
	}
	
	
	
	public static ArrayList<ArrayList<Object>> create_matrix(int n, Object value0, Object value1, Object value2){
		ArrayList<ArrayList<Object>> outer_list=new ArrayList<ArrayList<Object>>();
		//teeb 12 koopiat value1-st ja lisab need boundary-sse
		ArrayList<Object> boundary=new ArrayList<Object>(Collections.nCopies(n+2,value1));
		outer_list.add(boundary);
		//teeb veel 10 rida, kus on keskel value0-d ja m�lemal pool ��rtes value1
		for(int i=0;i<n;i++){
			ArrayList<Object> inner_list = new ArrayList<Object>(Collections.nCopies(n,value0));
			inner_list.add(0,value1);
			inner_list.add(value1);
			outer_list.add(inner_list);
		}
		//lisab outer_listi veel �he korra boundary-t, et moodustuks "raam"
		outer_list.add(boundary);
		set_fire(outer_list,value2);
		return outer_list;
	}
	
	//v�rvib maatriksi 6nda rea 6nda elemendi punaseks
	public static void set_fire(ArrayList<ArrayList<Object>> list, Object fire){
		int i=list.size()/2;
		list.get(i).set(i,fire);
		
	}
	
	//grid_matrix on create_matrix-i returnitud arraylist
	//see meetod s�tib grid_matrix-i listist maatriksiks
	public static void write_grid(GridPane grid,ArrayList<ArrayList<Object>> grid_matrix){
		for(int i=0;i<n;i++){
			for(int j=0;j<n;j++){
				Pane p=new Pane();
				p.setStyle(grid_matrix.get(i).get(j).toString());
				grid.add(p,i,j,2,2);
			}
		}
	}
	
	
	public static void waiting(){
		try{
			Thread.sleep(1000);
		}
		catch (InterruptedException ie){
			System.out.println("Exception!");
		}
	}
	
	//prindib elemendi ja t�hiku ning p�rast seda t�hja rea
	public static void write(ArrayList<ArrayList<Object>> matrix){
		for(ArrayList<Object> list:matrix){
			for(Object element:list){
				System.out.print(element+" ");
			}
			System.out.println();
		}
	}
	
	//v�rvib maatriksi elemente vastavalt sellele, mis v�rvi nende �mber olevad elemendid on
	public static void spread(ArrayList<ArrayList<Object>> matrix, Object value0, Object value1, Object value2, Object value3){
		int n=matrix.size();
		for(int i=1;i<n-1;i++){
			for(int j=1;j<n-1;j++){
				if(matrix.get(i).get(j)==value0){
					Object N=matrix.get(i-1).get(j);
					Object S=matrix.get(i+1).get(j);
					Object E=matrix.get(i).get(j+1);
					Object W=matrix.get(i).get(j-1);
					if(N==value2 || S==value2 || E==value2 || W==value2){
						if(Math.random()<probability){
							matrix.get(i).set(j,value3);
						}
					}
				}
			}
		}
		replace(matrix,value1,value2,value3);
	}
	
	
	public static void replace(ArrayList<ArrayList<Object>> matrix, Object value1, Object value2, Object value3){
		for(ArrayList<Object> list:matrix){
			Collections.replaceAll(list,value2,value1);
			Collections.replaceAll(list,value3,value2);
		}
	}
	
	
	public static void console(ArrayList<ArrayList<Object>> matrix){
		
		write(matrix);
		
		boolean burning=true;
		
		while (burning==true) {
			waiting();
			spread(matrix,0,1,2,3);
			System.out.println();
			write(matrix);
			for(ArrayList<Object> list:matrix){
				if(list.contains(2)){
					burning=true;
					break;
				}
				else
					burning=false;
			}
		}
	}
	

	public static void main(String[] args) {

		ArrayList<ArrayList<Object>> matrix=create_matrix(n,0,1,2);
		
		console(matrix);
		
		launch(args);
	}
}
